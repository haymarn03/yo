var sephora = angular.module("sephoraApp", [
  "ui.router",
  "oc.lazyLoad"
])
.constant("BaseAPI", "https://sephora-api-frontend-test.herokuapp.com")
.run(function ($rootScope) {
  $rootScope.$on('$locationChangeSuccess', function (event) {
    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
  });

});
;

sephora.config(["$ocLazyLoadProvider", function($ocLazyLoadProvider) {
  $ocLazyLoadProvider.config({
      // global configs go here
  });
}]);